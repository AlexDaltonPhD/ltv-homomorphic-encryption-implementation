import os
import time
from subprocess import call, check_output
import numpy as np
import math

program_time = []

if os.path.isfile("CEM_program_time.txt"):
    program_time = np.loadtxt("CEM_program_time.txt").tolist()

index = math.floor(len(program_time) / 12)
thread = len(program_time) % 12 + 1

while True:
    while thread < 13:
        print("Calculating ring order ", index, " threads ", thread)
        program_time = []
        os.environ["OMP_NUM_THREADS"] = str(thread)
        ring_order = 2**index
        out = check_output(["../implementation/build/CEM", str(ring_order)])
        data = out.splitlines()[2:]
        for i in range(len(data)):
            data[i] = int(data[i])
        program_time.append(sum(data)/len(data))
        print(sum(data)/len(data))
        with open("CEM_program_time.txt", "ab") as f:
            np.savetxt(f, program_time)
        thread = thread + 1
    index = index + 1
    thread = 1
