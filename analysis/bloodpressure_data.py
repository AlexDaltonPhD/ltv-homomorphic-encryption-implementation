import os
import time
from subprocess import call
import numpy as np
import math

program_time = []

if os.path.isfile("program_time.txt"):
    program_time = np.loadtxt("program_time.txt").tolist()

index = math.floor(len(program_time) / 12)
thread = len(program_time) % 12 + 1

while True:
    while thread < 13:
        print("Calculating ring order ", index, " threads ", thread)
        program_time = []
        os.environ["OMP_NUM_THREADS"] = str(thread)
        ring_order = 2**index
        start_time = time.time()
        call(["../implementation/build/bloodpressure", str(ring_order)])
        program_time.append(time.time() - start_time)
        with open("program_time.txt", "ab") as f:
            #f.write(b"\n")
            np.savetxt(f, program_time)
        thread = thread + 1
    index = index + 1
    thread = 1
