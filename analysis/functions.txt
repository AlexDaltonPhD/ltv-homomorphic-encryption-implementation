Clearly bounded by multiplication performance
Assuming no databottle neck (upper bound), expected a n^2d factor speedup given
unlimited hardware - this scales to hardware size
---

homomorphicBloodPressure : O(1) time, O(1024 * n * d) data
    -> O(10) calls: compareGreaterThanInt : O(8) time, O(512 * n * d) data
        -> O(2) calls: CRTEnc
        -> O(8) calls: CRTCompEvalMul
        -> O(8) calls: CRTEvalSub
        -> O(8) calls: CRTEvalAdd
    -> O(10) calls: streamAdderBit : O(1) time, O(512 * n * d) data
        -> O(1) calls: decomposeEnc : O(64) time, O(64 * 64 * n * d) data
            -> O(8) calls: CRTEnc
        -> O(1) calls: streamAdder
            -> O(1) calls: halfAdder : O(1) time, O(2 * 64 * n * d) data
                -> O(1) calls: CRTEvalAdd
                -> O(1) calls: CRTCompEvalMul
            -> O(7) calls: fullAdder : O(1) time, O(2 * 64 * n * d) data
                -> O(2) calls: halfAdder
                -> O(2) calls: CRTEvalAdd
                -> O(1) calls: CRTCompEvalMul

---

CRTEnc : O(1) time, O(n) data
    -> O(1) calls: Enc : O(n) time, O(500n) data
        -> O(1) calls: ringMul : O(1) time, O(1000n) data
            -> O(1) calls: polyMul : O(n^2) time, O(1000n) data
            -> O(1) calls: ringReduceModOrder : O(n) time, O(500n) data
        -> O(2) calls: ringAdd : O(2n) time, O(1000n) data
    -> O(1) calls: CRT : O(nd) time, O(n) data                                  : nd-parallel

CRTCompEvalMul : O(d) time, O(128nd) data
    -> O(1) calls: CRTEvalMul : O(1) time, O(128nd) data
        -> O(1) calls: CRTModMatch : O(2d) time, O(128nd) data
            -> O(d) calls : CRTModReduction : O(2dn+d+3n) time, O(128nd) data   : 2nd+3n-parallel
        -> O(1) calls: CRTRingMul : O(d + dn^2), O(128nd) data                  : d-parallel
            -> O(dn^2) calls: modMul : O(128) time, O(128) data                 : d-parallel calls
    -> O(1) calls: CRTKeySwitch : O(1) time, O(128nd) data
        -> O(1) calls: CRTRingMul : O(d + dn^2), O(128nd) data                  : d-parallel
            -> O(dn^2) calls: modMul : O(128) time, O(128) data                 : d-parallel calls
    -> O(1) calls: CRTModReduction : O(2nd+d+3n) time, O(128nd) data            : 2nd+3n-parallel

CRTEvalAdd : O(1) time, O(128nd) data
    -> O(1) calls: CRTModMatch : O(2d) time, O(128nd) data
        -> O(d) calls : CRTModReduction : O(2dn+d+3n) time, O(128nd) data       : 2nd+3n-parallel
    -> O(1) calls: CRTRingAdd : O(nd) time, O(128nd) data                       : nd-parallel

---
Variables: ring order, computation depth, operations a second, SoC/FPGA data transfer speed, FPGA speed?

n = ring order
d = computation dept
c = CPU cores

number of operations =

    10 * compareGreaterThanInt
    + 10 * streamAdderBit
    + c

    10 * (2 * CRTEnc + 8 * CRTCompEvalMul + 16 * CRTEvalAdd)
    + 10 * (decomposeEnc + streamAdder)
    + c

    10 * (2 * CRTEnc + 8 * CRTCompEvalMul + 16 * CRTEvalAdd)
    + 10 * (8 * CRTEnc + halfAdder + 7 * fullAdder)
    + c

    100 * CRTEnc + 80 * CRTCompEvalMul + 160 * CRTEvalAdd
    + 10 (CRTEvalAdd + CRTCompEvalMul + 7 * (2 * halfAdder + 2 * CRTEvalAdd + CRTCompEvalMul))
    + c

    100 * CRTEnc + 90 * CRTCompEvalMul + 170 * CRTEvalAdd
    + 70 * (4 * CRTEvalAdd + 3 * CRTCompEvalMul)
    + c

    100 * CRTEnc + 300 * CRTCompEvalMul + 450 * CRTEvalAdd + nd * constant

        CRTEnc  = Enc + CRT
                = n + ringMul + 2 * ringAdd + nd/c
                = n + polyMul + ringReduceModOrder + 4n + nd/c
                = 6n + n^2 + nd/c

        CRTCompEvalMul  = d + CRTEvalMul + CRTKeySwitch + CRTModReduction
                        = d + CRTModMatch + 2 * CRTRingMul + CRTModReduction
                        = 3d + (d + 1) * CRTModReduction + 2 * ((d+dn^2)/c + (dn^2)/c * modMul)
                        = 3d + (d + 1) * ((2nd+3n)/c + d) + 2d/c + (2dn^2)/c + (2dn^2)/c * 128
                        = 
