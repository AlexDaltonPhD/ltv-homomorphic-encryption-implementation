"""This python program is an execution model for the LTV cryptosystem"""
import math
#pylint: disable=too-many-public-methods
#pylint: disable=missing-docstring
#pylint: disable=C0103

class HomomorphicBloodPressure:
    """Class representing the execution of the homomorphicBloodPressure application"""
    def __init__(self, n: int, c: int):
        self.ring_order = n
        self.threads = c
        self.depth = 3 * 16 * 5
        self.integer_add = 1
        self.integer_mul = 3

    def security_parameter(self):
        return self.ring_order/self.ring_order

    def time(self):
        cycles = 2 * self.decomposeEnc() + 10 * self.compareGreaterThanInt()
        cycles += 10 * self.streamAdderBit() + 57000 * self.ring_order * self.depth
        return cycles / 2250000000

    def polyMul(self):
        return self.ring_order**2 * 100 + 200 * self.ring_order + 100

    def ringReduceModOrder(self):
        return self.ring_order  * 50 + self.ring_order * 1000 + 135 * self.ring_order

    def ringMul(self):
        return self.polyMul() + self.ringReduceModOrder() + 135 * self.ring_order * 2

    def ringAdd(self):
        return 2 * self.ring_order * 15 + self.ringReduceModOrder() + 135 * self.ring_order

    def Enc(self):
        cycles = 135 * 2 * self.ring_order + 500 + self.ring_order * 300
        cycles += self.ringMul() + 2 * self.ringAdd()
        return cycles

    def CRT(self):
        cycles = 20 * self.ring_order * self.depth
        cycles += 1000 * math.ceil((self.ring_order * self.depth)/self.threads)
        return cycles

    def CRTEnc(self):
        return self.Enc() + self.CRT() + 650 * self.ring_order + 1000

    def coefReduction(self):
        return 500 + 20 * self.ring_order + math.ceil(self.ring_order/self.threads) * 1000

    def CRTModReduction(self):
        cycles = 50 * self.ring_order + 150
        cycles += 300 * math.ceil(self.ring_order/self.threads)
        cycles += self.coefReduction()
        cycles += 600
        cycles += 1500 * math.ceil(self.ring_order/self.threads)
        cycles += self.CRT()
        cycles += 1000 * self.depth / 2
        cycles += 200 * math.ceil(self.ring_order/self.threads) * self.depth / 2
        cycles += 5000 * self.depth / 2
        cycles += 2000 * math.ceil(self.ring_order/self.threads) * self.depth / 2 + 3000
        return cycles

    def CRTRingMul(self):
        cycles = 80 + self.ring_order * 24 + self.ring_order**2 * 2250
        return cycles * math.ceil(self.depth/(2*self.threads))

    def CRTKeySwitch(self):
        return 50 + self.CRTRingMul()

    def CRTModMatch(self):
        return 60 * self.CRTModReduction()

    def CRTEvalMul(self):
        return 1500 + self.CRTModMatch() + self.CRTRingMul()

    def CRTCompEvalMul(self):
        return self.CRTEvalMul() + self.CRTKeySwitch() + self.CRTModReduction()

    def CRTRingAdd(self):
        return math.ceil((self.ring_order * self.depth) / self.threads) * 30

    def CRTEvalAdd(self):
        return 1500 + self.CRTModMatch() + self.CRTRingAdd()

    def halfAdder(self):
        return self.CRTEvalAdd() + self.CRTCompEvalMul()

    def fullAdder(self):
        cycles = 100 * self.ring_order * self.depth + 2 * self.halfAdder()
        cycles += 2 * self.CRTEvalAdd() + self.CRTCompEvalMul()
        return cycles

    def streamAdder(self):
        cycles = self.halfAdder() + 7 * self.fullAdder()
        cycles += math.ceil(8/self.threads) * (self.ring_order * self.depth * 20 + 800)
        return cycles

    def decomposeEnc(self):
        return 64 + 8 * self.CRTEnc()

    def streamAdderBit(self):
        return self.decomposeEnc() + self.streamAdder()

    def compareGreaterThanInt(self):
        return 8 + 2 * self.CRTEnc() + 8 * self.CRTCompEvalMul() + 8 * self.CRTEvalAdd()
