#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <omp.h>
#include "../libltv/impl_constructions.h"

uint64_t get_cycles(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

int main(int argc, char *argv[])
{
    int ring_order = 8;
    if(argc > 1) ring_order = atoi(argv[1]);
    Parameters* p = generateParameters(ring_order, 2, 3 * 16 * 5);
    KeyPair* keys = KeyGen(p);

    PlainText* a_m = newPlainText(p->n);
    PlainText* b_m = newPlainText(p->n);
    
	DblCRT hint = CRTKeySwitchHint(2, keys->sk, keys->sk, p);

    mpz_t m_mod;
    mpz_init(m_mod);
    mpz_set_ui(m_mod, 2);
	mpz_t delta, scratch;
	mpz_init(delta);
	mpz_init(scratch);

	for(int i = 0; i < 30; i++)
	{
		for(uint64_t i = 0; i < p->n; i++)
		{
			mpz_urandomm(a_m->co[i], p->state, m_mod);
			mpz_urandomm(b_m->co[i], p->state, m_mod);
		}

		DblCRTCipherText a_c = CRTEnc(a_m, keys->pk, p);
		DblCRTCipherText b_c = CRTEnc(b_m, keys->pk, p);
    	DblCRTCipherText out = CRTEnc(b_m, keys->pk, p);

		struct timespec start, end;
		clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    	CRTCompEvalMul(&out, &a_c, &b_c, hint, p);
		clock_gettime(CLOCK_MONOTONIC_RAW, &end);

		mpz_set_ui(delta, end.tv_sec);
		mpz_sub_ui(delta, delta, start.tv_sec);
		mpz_mul_ui(delta, delta, 1000);
		mpz_set_si(scratch, (end.tv_nsec - start.tv_nsec) / 1000000);
		mpz_add(delta, delta, scratch);
		gmp_printf("%Zd\n", delta);

		CRTDec(a_m, a_c, keys->sk, p);
		CRTDec(a_m, b_c, keys->sk, p);
		CRTDec(a_m, out, keys->sk, p);
	}
	
	mpz_clear(delta);
	mpz_clear(scratch);
	freePlainText(a_m);
	freePlainText(b_m);
	freeParameters(p);
}
