#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "../libltv/impl_constructions.h"

void homomorphicBloodPressure(CipherTextSet** systolic_class,
        CipherTextSet** diastolic_class,
        CipherTextSet* systolic,
        CipherTextSet* diastolic,
        PublicKey pk,
        DblCRT key_switch_hint,
        Parameters* p)
{
    uint64_t sys_ref[5] = { 90, 120, 140, 160, 180 };
    uint64_t dia_ref[5] = { 60, 80, 90, 100, 110 };

    *systolic_class = decomposeEnc(0, pk, p, 8);
    *diastolic_class = decomposeEnc(0, pk, p, 8);

    for(int i = 0; i < 5; i++)
    {
        DblCRTCipherText scratch1 = compareGreaterThanInt(systolic, sys_ref[i], pk, key_switch_hint, p);
        streamAdderBit(*systolic_class, **systolic_class, scratch1, key_switch_hint, pk, p);
        freeDblCRTCipherText(scratch1);

        DblCRTCipherText scratch2 = compareGreaterThanInt(diastolic, dia_ref[i], pk, key_switch_hint, p);
        streamAdderBit(*diastolic_class, **diastolic_class, scratch2, key_switch_hint, pk, p);
        freeDblCRTCipherText(scratch2);
    }
}

int main(int argc, char *argv[])
{
    int ring_order = 8;
    if(argc > 1) ring_order = atoi(argv[1]);
    Parameters* p = generateParameters(ring_order, 2, 3 * 16 * 5);
    KeyPair* keys = KeyGen(p);

    uint64_t m_sys_bp = gmp_urandomm_ui(p->state, 110) + 80;
    uint64_t m_dia_bp = gmp_urandomm_ui(p->state, 70) + 50;
    CipherTextSet* c_sys_bp = decomposeEnc(m_sys_bp, keys->pk, p, 8);
    CipherTextSet* c_dia_bp = decomposeEnc(m_dia_bp, keys->pk, p, 8);

    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);
    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    CipherTextSet* enc_sys_result;
    CipherTextSet* enc_dia_result;


    // ========== UNTRUSTED SERVER ==========
    homomorphicBloodPressure(&enc_sys_result, &enc_dia_result, c_sys_bp, c_dia_bp, keys->pk, hint, p);
    // ========== UNTRUSTED SERVER ==========

    uint64_t sys_class = composeDec(enc_sys_result, keys->sk, p);
    uint64_t dia_class = composeDec(enc_dia_result, keys->sk, p);

    switch(sys_class)
    {
        case 0 :
            printf("Systolic %d : hypotension\n", m_sys_bp);
            break;
        case 1 :
            printf("Systolic %d : desired\n", m_sys_bp);
            break;
        case 2 :
            printf("Systolic %d : prehypertension\n", m_sys_bp);
            break;
        case 3 :
            printf("Systolic %d : stage 1 hypertension\n", m_sys_bp);
            break;
        case 4 :
            printf("Systolic %d : stage 2 hypertension\n", m_sys_bp);
            break;
        case 5 :
            printf("Systolic %d : hypertensive emergency\n", m_sys_bp);
            break;
    }

    switch(dia_class)
    {
        case 0 :
            printf("Diastolic %d : hypotension\n", m_dia_bp);
            break;
        case 1 :
            printf("Diastolic %d : desired\n", m_dia_bp);
            break;
        case 2 :
            printf("Diastolic %d : prehypertension\n", m_dia_bp);
            break;
        case 3 :
            printf("Diastolic %d : stage 1 hypertension\n", m_dia_bp);
            break;
        case 4 :
            printf("Diastolic %d : stage 2 hypertension\n", m_dia_bp);
            break;
        case 5 :
            printf("Diastolic %d : hypertensive emergency\n", m_dia_bp);
            break;
    }
    freeParameters(p);
    freePoly(&joint_sk);
    freeDblCRT(hint);

    return 0;
}
