add_library(libltv SHARED parameters.c gmp_impl.c gmp_ring_arithmetic.c crt.c impl_constructions.c)
find_package(OpenMP)
if(OpenMP_C_FOUND)
    target_link_libraries(libltv PUBLIC OpenMP::OpenMP_C)
endif()

