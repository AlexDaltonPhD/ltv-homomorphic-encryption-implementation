#include "gmp_impl.h"
#include <stdlib.h>

PlainText* newPlainText(const int order)
{
    PlainText* p = (PlainText*) malloc(sizeof(PlainText));
    p->order = order;
    initPoly(p);

    return p;
}

CipherText* newCipherText(const int order, const mpz_t q)
{
    CipherText* c = (CipherText*) malloc(sizeof(CipherText));
    mpz_init(c->mod);
    mpz_set(c->mod, q);

    c->poly.order = order;
    initPoly(&(c->poly));
    c->degree = 0;
}

void freePlainText(PlainText* p)
{
    freePoly(p);
    free((PlainText*) p);
}

void freeCipherText(CipherText* c)
{
    freePoly(&(c->poly));
    mpz_clear(c->mod);
    free((CipherText*) c);
}

void coefReduction(Polynomial out, const Polynomial in, const mpz_t mod)
{
    mpz_t half_q;
    mpz_init(half_q);
    mpz_sub_ui(half_q, mod, 1);
    mpz_fdiv_q_ui(half_q, half_q, 2);
    
    Polynomial temp = { NULL, in.order };
    initPoly(&temp);

#pragma omp parallel for
    for(int i = 0; i < in.order; i++)
    {
        mpz_set(temp.co[i], in.co[i]);
        if(mpz_cmp(temp.co[i], half_q) >= 0)
        {
            mpz_sub(temp.co[i], temp.co[i], mod);
        }
        mpz_set(out.co[i], temp.co[i]);
    }

    mpz_clear(half_q);
    freePoly(&temp);
}

KeyPair* KeyGen(Parameters* p)
{
    printf("Generating keys...");
    KeyPair* keys = (KeyPair*) malloc(sizeof(KeyPair));
    keys->sk.order = p->n;
    keys->pk.order = p->n;

    initPoly(&(keys->sk));
    initPoly(&(keys->pk));

    Polynomial u = { NULL, p->n };
    Polynomial f_inv = { NULL, p->n };

    initPoly(&u);
    initPoly(&f_inv);

    int invertable = 0;
    mpz_t B;
    mpz_init(B);
    mpz_urandomb(B, p->state, 8);
    while(mpz_cmp_ui(B, 0) == 0) mpz_urandomb(B, p->state, 8);

    //sk = 2 * f' + 1
    while(!invertable)
    {
        for(int i = 0; i < p->n; i++)
        {
            mpz_urandomm(keys->sk.co[i], p->state, B);
            mpz_mul_ui(keys->sk.co[i], keys->sk.co[i], p->p);
        }
        mpz_add_ui(keys->sk.co[0], keys->sk.co[0], 1);

        invertable = ringInv(f_inv, keys->sk, p);
    }

    // pk = 2 * g * f^-1
    for(int i = 0; i < p->n; i++)
    {
        mpz_urandomm(u.co[i], p->state, B);
        mpz_mul_ui(u.co[i], u.co[i], p->p);
    }

    ringMul(keys->pk, u, f_inv, p);

    freePoly(&u);
    freePoly(&f_inv);
    mpz_clear(B);
    printf("done.\n");
    return keys;
}

void Enc(CipherText* c, const PlainText* m, const PublicKey pk, Parameters* p)
{
    Polynomial e = { NULL, p->n };
    Polynomial s = { NULL, p->n };

    initPoly(&e);
    initPoly(&s);

    mpz_t B;
    mpz_init(B);
    mpz_urandomb (B, p->state, 8);
    while(mpz_cmp_ui(B, 0) == 0) mpz_urandomb (B, p->state, 10); 

    // c = pk * s + 2 * e + m
    for(int i = 0; i < p->n; i++)
    {
        mpz_urandomm(e.co[i], p->state, B);
        mpz_mul_ui(e.co[i], e.co[i], p->p);
        mpz_urandomm(s.co[i], p->state, B);
    }

    ringMul(s, s, pk, p);
    ringAdd(e, e, *m, p);
    ringAdd(c->poly, s, e, p);

    c->degree = 1;
    mpz_set(c->mod, p->q);

    mpz_clear(B);
    freePoly(&e);
    freePoly(&s);
}

void Dec(PlainText* m, const CipherText* c, const SecretKey sk, const Parameters* p)
{
    Polynomial b = { NULL, p->n };
    Polynomial f = { NULL, p->n };
    initPoly(&b);
    initPoly(&f);

    copyPoly(f, sk);
    for(int i = 1; i < c->degree; i++) ringMulModOrder(f, f, sk, c->mod, p->n);

    // m = [fc]_q (mod p)
    ringMulModOrder(b, sk, c->poly, c->mod, p->n);
    coefReduction(b, b, c->mod);
    for(int i = 0; i < p->n; i++)
    {
        mpz_mod_ui(m->co[i], b.co[i], p->p);
    }

    freePoly(&b);
}

void EvalAdd(CipherText* out, CipherText* a, CipherText* b, const Parameters* p)
{
    modMatch(a, b, p);
    ringAddModOrder(out->poly, a->poly, b->poly, a->mod, p->n);
    out->degree = a->degree > b->degree ? a->degree : b->degree;
    mpz_set(out->mod, a->mod);
}

void EvalSub(CipherText* out, CipherText* a, CipherText* b, const Parameters* p)
{
    modMatch(a, b, p);
    ringSubModOrder(out->poly, a->poly, b->poly, a->mod, p->n);
    out->degree = a->degree > b->degree ? a->degree : b->degree;
    mpz_set(out->mod, a->mod);
}

void EvalMul(CipherText* out, CipherText* a, CipherText* b, const Parameters* p)
{
    modMatch(a, b, p);
    ringMulModOrder(out->poly, a->poly, b->poly, a->mod, p->n);
    out->degree = a->degree + b->degree;
    mpz_set(out->mod, a->mod);
}

void KeySwitchHint(Polynomial hint, int degree, const SecretKey f_1, const SecretKey f_2, Parameters* p)
{
    Polynomial f_2_inv = { NULL, p->n };
    Polynomial m = { NULL, p->n };
    initPoly(&f_2_inv);
    initPoly(&m);

    ringInv(f_2_inv, f_2, p);

    mpz_t B;
    mpz_init(B);
    mpz_urandomb(B, p->state, 8);
    while(mpz_cmp_ui(B, 0) == 0) mpz_urandomb (B, p->state, 8); 

    for(int i = 0; i < p->n; i++)
    {
        mpz_urandomm(m.co[i], p->state, B);
        mpz_mul_ui(m.co[i], m.co[i], p->p);
    }
    mpz_add_ui(m.co[0], m.co[0], 1);
    mpz_clear(B);

    // hint = m * f_1 ^ d * f_2 ^ -1
    ringMul(hint, m, f_2_inv, p);
    for(int i = 0; i < degree; i++) ringMul(hint, hint, f_1, p);

    freePoly(&f_2_inv);
    freePoly(&m);
}

void KeySwitch(CipherText* out, const CipherText* in, const Polynomial hint, const Parameters* p)
{
    ringMul(out->poly, hint, in->poly, p);
    out->degree = 1;
}

void Decompose(CipherText* out_a, CipherText* out_b, CipherText* in, Parameters *p)
{
    CipherText* temp_a = newCipherText(p->n / 2, p->q);
    CipherText* temp_b = newCipherText(p->n / 2, p->q);

    for(int i = 0; i < p->n / 2; i++)
    {
        mpz_set(temp_a->poly.co[i], in->poly.co[2 * i]);
        mpz_set(temp_b->poly.co[i], in->poly.co[2 * i + 1]);
    }

    copyPoly(out_a->poly, temp_a->poly);
    copyPoly(out_b->poly, temp_b->poly);

    freeCipherText(temp_a);
    freeCipherText(temp_b);
}

void modMatch(CipherText* a, CipherText *b, const Parameters* p)
{
    mpz_t r_a, r_b;
    mpz_init(r_a);
    mpz_init(r_b);
    for(int i = 0; i < p->depth; i++)
    {
        mpz_mod(r_a, a->mod, p->q_factors[i]);
        mpz_mod(r_b, b->mod, p->q_factors[i]);

        if(mpz_cmp_ui(r_a, 0) == 0 && mpz_cmp_ui(r_b, 0) != 0) ModReduction(a, a, p->q_factors[i], p->inv_q_factors[i], p);
        if(mpz_cmp_ui(r_a, 0) != 0 && mpz_cmp_ui(r_b, 0) == 0) ModReduction(b, b, p->q_factors[i], p->inv_q_factors[i], p);
    }
    mpz_clear(r_a);
    mpz_clear(r_b);
}

void ModReduction(CipherText* out, const CipherText* in, const mpz_t q_prime, const mpz_t v, const Parameters* p)
{
    Polynomial d = { NULL, in->poly.order };
    initPoly(&d);

    mpz_t scratch;
    mpz_init(scratch);
    mpz_mul(scratch, v, q_prime);
    mpz_sub_ui(scratch, scratch, 1);

    for(int i = 0; i < d.order; i++)
    {
        mpz_mod(d.co[i], in->poly.co[i], q_prime);
        mpz_mul(d.co[i], d.co[i], scratch);
    }
    mpz_mul_ui(scratch, q_prime, p->p);
    for(int i = 0; i < d.order; i++) mpz_mod(d.co[i], d.co[i], scratch);
    coefReduction(d, d, scratch);

    polyAdd(d, d, in->poly);
    ringReduceModOrder(d, d, in->mod, in->poly.order);

    for(int i = 0; i < d.order; i++)
    {
        mpz_fdiv_q(d.co[i], d.co[i], q_prime);
    }

    copyPoly(out->poly, d);
    mpz_div(out->mod, in->mod, q_prime);

    freePoly(&d);
    mpz_clear(scratch);
}

void RingReduction(CipherText* out, const CipherText* in, const Polynomial sparse_hint, Parameters *p)
{
    CipherText* temp = newCipherText(in->poly.order, in->mod);
    CipherText* sparse_temp = newCipherText(in->poly.order / 2, in->mod);

    KeySwitch(temp, in, sparse_hint, p);

    for(int i = 0; i < in->poly.order / 2; i++) mpz_set(sparse_temp->poly.co[i], temp->poly.co[2 * i]);

    for(int i = out->poly.order; i > sparse_temp->poly.order; i--) mpz_clear(out->poly.co[i - 1]);
    out->poly.order = sparse_temp->poly.order;

    copyPoly(out->poly, sparse_temp->poly);
    p->n = p->n / 2;

    freeCipherText(temp);
    freeCipherText(sparse_temp);
}

void CompEvalMul(CipherText* out, CipherText* a, CipherText* b, const Polynomial keyswitch_hint, const Parameters *p)
{
    EvalMul(out, a, b, p);
    KeySwitch(out, out, keyswitch_hint, p);
    
    mpz_t r;
    mpz_init(r);
    int i = 0;
    
    while(i < p->depth)
    {
        mpz_mod(r, out->mod, p->q_factors[i]);
        if(mpz_cmp_ui(r, 0) == 0) break;
        i++;
    }
    mpz_clear(r);
    ModReduction(out, out, p->q_factors[i], p->inv_q_factors[i], p);
}
