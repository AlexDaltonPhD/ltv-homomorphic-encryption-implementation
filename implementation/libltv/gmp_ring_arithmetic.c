#include "gmp_impl.h"
#include <stdlib.h>
#include <x86intrin.h>

void initPoly(Polynomial* p)
{
    p->co = (mpz_t*) malloc(sizeof(mpz_t) * p->order);
    for(int i = 0; i < p->order; i++)
    {
        mpz_init(p->co[i]);
    }
}

void freePoly(Polynomial* p)
{
    for(int i = 0; i < p->order; i++)
    {
        mpz_clear(p->co[i]);
    }
    free(p->co);
}

void copyPoly(Polynomial output, const Polynomial input)
{
    for(int i = 0; i < output.order; i++)
    {
        if(i < input.order) mpz_set(output.co[i], input.co[i]);
        else mpz_set_ui(output.co[i], 0);
    }
}

void setPoly(Polynomial output, const uint64_t input)
{
    for(int i = 0; i < output.order; i++)
    {
        mpz_set_ui(output.co[i], input);
    }
}

void printPoly(const Polynomial p)
{
    printf("[ ");
    for(int i = p.order - 1; i >= 0; i--) gmp_printf("%Zd ", p.co[i]);
    printf("]\n");
}

void polyMod(Polynomial output, const mpz_t q)
{
    for(int i = 0; i < output.order; i++)
    {
        mpz_mod(output.co[i], output.co[i], q);
    }
}

int modInv(mpz_t output, const mpz_t input, const mpz_t q)
{
    mpz_t t, r, new_t, new_r;
    mpz_init(t);
    mpz_init(r);
    mpz_init(new_t);
    mpz_init(new_r);

    mpz_set_ui(t, 0);
    mpz_set(r, q);
    mpz_set_ui(new_t, 1);
    mpz_set(new_r, input);

    mpz_t quotient;
    mpz_t scratch;
    mpz_init(quotient);
    mpz_init(scratch);

    while(mpz_cmp_ui(new_r, 0) != 0)
    {
        mpz_fdiv_q(quotient, r, new_r);
        mpz_set(scratch, t);
        mpz_set(t, new_t);
        mpz_mul(new_t, new_t, quotient);
        mpz_sub(new_t, scratch, new_t);

        mpz_set(scratch, r);
        mpz_set(r, new_r);
        mpz_mul(new_r, new_r, quotient);
        mpz_sub(new_r, scratch, new_r);
    }

    int ret = 1;

    if(mpz_cmp_ui(r, 1) > 0)
    {
        ret = 0;
    }

    if(mpz_cmp_ui(t, 0) < 0)
    {
        mpz_add(t, t, q);
    }

    mpz_set(output, t);

    mpz_clear(quotient);
    mpz_clear(scratch);
    mpz_clear(t);
    mpz_clear(r);
    mpz_clear(new_t);
    mpz_clear(new_r);

    return ret;
}

void ringReduce(Polynomial output, const Polynomial input, const Parameters* p)
{
    Polynomial scratch = { NULL, p->n };
    initPoly(&scratch);

    // polynomial reduction: x^n + 1
    setPoly(scratch, 0);

    for(int i = 0; i < input.order; i++)
    {
        if(i < p->n)
        {
            mpz_add(scratch.co[i], scratch.co[i], input.co[i]);
        }
        else
        {
            mpz_sub(scratch.co[i % p->n], scratch.co[i % p->n], input.co[i]);
        }
    }

    // modulus p->q
    for(int i = 0; i < p->n; i++)
    {
        mpz_mod(output.co[i], scratch.co[i], p->q);
    }

    freePoly(&scratch);
}

void ringReduceModOrder(Polynomial output, const Polynomial input, const mpz_t mod, const int order)
{
    Polynomial scratch = { NULL, order };
    initPoly(&scratch);

    // polynomial reduction: x^n + 1
    setPoly(scratch, 0);

    for(int i = 0; i < input.order; i++)
    {
        if(i < order)
        {
            mpz_add(scratch.co[i], scratch.co[i], input.co[i]);
        }
        else
        {
            mpz_sub(scratch.co[i % order], scratch.co[i % order], input.co[i]);
        }
    }

    // modulus mod
    for(int i = 0; i < order; i++)
    {
        mpz_mod(output.co[i], scratch.co[i], mod);
    }

    freePoly(&scratch);
}

void polyAdd(Polynomial output, const Polynomial a, const Polynomial b)
{
    uint64_t output_order = a.order > b.order ? a.order : b.order;
    Polynomial scratch = { NULL, output_order };
    initPoly(&scratch);

    for(int i = 0; i < output_order; i++)
    {
        mpz_set_ui(scratch.co[i], 0);
        if(i < a.order) mpz_add(scratch.co[i], scratch.co[i], a.co[i]);
        if(i < b.order) mpz_add(scratch.co[i], scratch.co[i], b.co[i]);
    }

    copyPoly(output, scratch);

    freePoly(&scratch);
}

void ringAdd(Polynomial output, const Polynomial a, const Polynomial b, const Parameters* p)
{
    polyAdd(output, a, b);
    ringReduce(output, output, p);
}

void ringAddModOrder(Polynomial output, const Polynomial a, const Polynomial b, const mpz_t mod, const int order)
{
    polyAdd(output, a, b);
    ringReduceModOrder(output, output, mod, order);
}

void polySub(Polynomial output, const Polynomial a, const Polynomial b)
{
    uint64_t output_order = a.order > b.order ? a.order : b.order;
    Polynomial scratch = { NULL, output_order };
    initPoly(&scratch);

    for(int i = 0; i < output_order; i++)
    {
        mpz_set_ui(scratch.co[i], 0);
        if(i < a.order) mpz_add(scratch.co[i], scratch.co[i], a.co[i]);
        if(i < b.order) mpz_sub(scratch.co[i], scratch.co[i], b.co[i]);
    }

    copyPoly(output, scratch);

    freePoly(&scratch);
}

void ringSub(Polynomial output, const Polynomial a, const Polynomial b, const Parameters* p)
{
    polySub(output, a, b);
    ringReduce(output, output, p);
}

void ringSubModOrder(Polynomial output, const Polynomial a, const Polynomial b, const mpz_t mod, const int order)
{
    polySub(output, a, b);
    ringReduceModOrder(output, output, mod, order);
}

void polyMul(Polynomial output, const Polynomial a, const Polynomial b)
{
    Polynomial output_temp = { NULL, a.order + b.order };
    initPoly(&output_temp);
    setPoly(output_temp, 0);

    mpz_t scratch;
    mpz_init(scratch);
    for(uint64_t i = 0; i < a.order; i++)
    {
        for(uint64_t j = 0; j < b.order; j++)
        {
            mpz_mul(scratch, a.co[i], b.co[j]);
            mpz_add(output_temp.co[i + j], output_temp.co[i + j], scratch);
        }
    }

    copyPoly(output, output_temp);

    freePoly(&output_temp);
    mpz_clear(scratch);
}

void ringMul(Polynomial output, const Polynomial a, const Polynomial b, const Parameters* p)
{
    ringMulModOrder(output, a, b, p->q, p->n);
}

void ringMulModOrder(Polynomial output, const Polynomial a, const Polynomial b, const mpz_t mod, const int order)
{
    Polynomial output_temp = { NULL, 2 * order };
    initPoly(&output_temp);

    polyMul(output_temp, a, b);
    ringReduceModOrder(output, output_temp, mod, order);
    
    freePoly(&output_temp);
}

int degree(const Polynomial a)
{
    for(int i = a.order - 1; i >= 0; i--)
    {
        if(mpz_cmp_ui(a.co[i], 0) != 0) return i;
    }
    return -1;
}

int zero(const Polynomial a)
{
    for(int i = 0; i < a.order; i++)
    {
        if(mpz_cmp_ui(a.co[i], 0) != 0)
        {
            return 0;
        }
    }
    return 1;
}

// IEEE standard 1363.1-2008 
int polyDiv(Polynomial quotient, Polynomial remainder, const Polynomial a, const Polynomial b, const mpz_t m)
{
    mpz_t u;
    Polynomial r = { NULL, a.order };
    Polynomial q = { NULL, a.order };
    Polynomial v = { NULL, a.order + b.order };
    initPoly(&r);
    initPoly(&q);
    initPoly(&v);
    mpz_init(u);

    copyPoly(r, a);
    setPoly(q, 0);

    int N = degree(b);

    int ret = 0;

    if(!zero(b) && modInv(u, b.co[N], m)) 
    {
        while(degree(r) >= N)
        {
            int d = degree(r);

            setPoly(v, 0);
            mpz_mul(v.co[d - N], u, r.co[d]);
            mpz_mod(v.co[d - N], v.co[d - N], m);

            polyAdd(q, q, v);
            polyMul(v, v, b);
            polySub(r, r, v);

            for(int i = 0; i < a.order; i++)
            {
                mpz_mod(q.co[i], q.co[i], m);
                mpz_mod(r.co[i], r.co[i], m);
            }
        }

        ret = 1;
    }

    copyPoly(quotient, q);
    copyPoly(remainder, r);

    freePoly(&r);
    freePoly(&q);
    freePoly(&v);
    mpz_clear(u);

    return ret;
}

int extEuclidean(Polynomial u, Polynomial v, Polynomial d, const Polynomial a, const Polynomial b, const mpz_t m)
{
    if(zero(a) && zero(b)) return 0;
    
    setPoly(u, 0);
    setPoly(v, 0);
    mpz_set_ui(u.co[0], 1);
    copyPoly(d, a);

    if(zero(b)) return 1;

    Polynomial v_1 = { NULL, v.order };
    Polynomial v_3 = { NULL, v.order };
    Polynomial q = { NULL, d.order };
    Polynomial t_1 = { NULL, a.order };
    Polynomial t_3 = { NULL, a.order };
    Polynomial scratch = { NULL, a.order * 2};

    initPoly(&v_1);
    initPoly(&v_3);
    initPoly(&t_1);
    initPoly(&t_3);
    initPoly(&q);
    initPoly(&scratch);

    setPoly(v_1, 0);
    copyPoly(v_3, b);

    while(!zero(v_3))
    {
        if(polyDiv(q, t_3, d, v_3, m) == 0)
        {
            freePoly(&v_1);
            freePoly(&v_3);
            freePoly(&t_1);
            freePoly(&t_3);
            freePoly(&q);
            freePoly(&scratch);
            return 0;
        }
        polyMod(q, m);
        polyMod(t_3, m);

        polyMul(scratch, q, v_1);
        polyMod(scratch, m);
        polySub(t_1, u, scratch);
        polyMod(t_1, m);

        copyPoly(u, v_1);
        copyPoly(d, v_3);
        copyPoly(v_1, t_1);
        copyPoly(v_3, t_3);
    }

    polyMul(scratch, a, u);
    polySub(scratch, d, scratch);
    polyDiv(v, t_3, scratch, b, m);

    polyMod(v, m);
    polyMod(u, m);
    polyMod(d, m);

    freePoly(&v_1);
    freePoly(&v_3);
    freePoly(&t_1);
    freePoly(&t_3);
    freePoly(&q);
    freePoly(&scratch);
    return 1;
}

int ringInv(Polynomial a_inv, const Polynomial a, const Parameters* p)
{
    Polynomial u = { NULL, p->n + 1 };
    Polynomial v = { NULL, p->n + 1 };
    Polynomial d = { NULL, p->n + 1 };
    Polynomial b = { NULL, p->n + 1 };

    initPoly(&u);
    initPoly(&v);
    initPoly(&d);
    initPoly(&b);

    setPoly(b, 0);
    mpz_set_ui(b.co[0], 1);
    mpz_set_ui(b.co[p->n], 1);

    int ret = 0;

    extEuclidean(u, v, d, a, b, p->q);

    if(degree(d) == 0)
    {
        ret = 1;
        modInv(d.co[0], d.co[0], p->q);
        polyMul(a_inv, d, u);
    }

    freePoly(&u);
    freePoly(&v);
    freePoly(&d);
    freePoly(&b);

    return ret;
}
