#ifndef GMP_RING_ARITHMETIC_H
#define GMP_RING_ARITHMETIC_H
/******************************************************************************
* File:             gmp_ring_arithmetic.h
*
* Author:           Alex Dalton
* Created:          12/04/19 
* Description:      LTV encryption implimentation using GNU Multiple Precission
*                   library.
*                   Ref: 1363.1-2008 - IEEE Standard Specification for Public Key
*                   Cryptographic Techniques Based on Hard Problems over Lattices
*****************************************************************************/
#include <stdio.h>
#include <gmp.h>
#include "parameters.h"

typedef struct Polynomial {
    mpz_t* co;          // coefficients of the polynomial, indexed by order
    uint64_t order;     // maximum polynomial order
} Polynomial;

/**
 * initPoly initialises a pointer to PlainText or CipherText given parameters p
 */
void initPoly(Polynomial* p);

/**
 * freePoly deallocates a PlainText or CipherText
 */
void freePoly(Polynomial* p);

/**
 * copyPoly copies one Polynomial over another, without exceding the order of the output polynomial
 * setPoly sets the coefficients of a polynomial all to the same unsigned integer
 * printPoly prints the polynomial p
 */
void copyPoly(Polynomial output, const Polynomial input);
void setPoly(Polynomial output, const uint64_t input);
void printPoly(const Polynomial p);

/**
 * zero returns 1 if a polynomial is 0
 */
int zero(const Polynomial a);

/**
 * degree returns the degree of the polynomial a
 */
int degree(const Polynomial a);

/**
 * modInv find the inverse of input modulo q using the extended Euclidian algorithm, returns 1 if invertable,
 * 0 otherwise
 */
int modInv(mpz_t output, const mpz_t input, const mpz_t q);

/**
 * polyAdd calculates a polynomial output s.t output = a + b
 * polySub calculates a polynomial output s.t output = a - b
 * polyMul calculates a polynomial output s.t output = a * b
 * polyDiv calculates a polynomail quotient and remainder s.t a = quotient * b + remainder (mod m) and returns 0 if a is irreducable, 1 otherwise
 */
void polyAdd(Polynomial output, const Polynomial a, const Polynomial b);
void polySub(Polynomial output, const Polynomial a, const Polynomial b);
void polyMul(Polynomial output, const Polynomial a, const Polynomial b);
int polyDiv(Polynomial quotient, Polynomial remainder, const Polynomial a, const Polynomial b, const mpz_t m);

/**
 * ringReduce reduces a polynomial (input) to a ring defined by Parameters p or by a defined modulus and order
 */
void ringReduce(Polynomial output, const Polynomial input, const Parameters* p);
void ringReduceModOrder(Polynomial output, const Polynomial input, const mpz_t mod, const int order);

/**
 * ringAdd calculates output = a + b for polynomials under parameter p in a ring
 * ringSub calculates output = a - b for polynomials under parameter p in a ring
 * ringMul calculates output = a * b for polynomials under parameter p in a ring
 * ringDiv calculates output = a / b for polynomials under parameter p in a ring
 */
void ringAdd(Polynomial output, const Polynomial a, const Polynomial b, const Parameters* p);
void ringAddModOrder(Polynomial output, const Polynomial a, const Polynomial b, const mpz_t mod, const int order);
void ringSub(Polynomial output, const Polynomial a, const Polynomial b, const Parameters* p);
void ringSubModOrder(Polynomial output, const Polynomial a, const Polynomial b, const mpz_t mod, const int order);
void ringMul(Polynomial output, const Polynomial a, const Polynomial b, const Parameters* p);
void ringMulModOrder(Polynomial output, const Polynomial a, const Polynomial b, const mpz_t mod, const int order);
void ringDiv(Polynomial quotient, Polynomial remainder, const Polynomial a, const Polynomial b, const Parameters* p);

/**
 * extEuclidean finds polynomials u, v, and d; s.t. a * u + b * v = d (mod m)
 */
int extEuclidean(Polynomial u, Polynomial v, Polynomial d, const Polynomial a, const Polynomial b, const mpz_t m);

/**
 * ringInv find the inverse of polynomial a in the ring defined by p, s.t a * a_inv = 1
 */
int ringInv(Polynomial a_inv, const Polynomial a, const Parameters* p);

#endif /* GMP_RING_ARITHMETIC_H */
