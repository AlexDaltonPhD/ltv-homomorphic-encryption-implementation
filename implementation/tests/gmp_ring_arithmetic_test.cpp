#include <gtest/gtest.h>
#include <gmp.h>

extern "C" {
#include "../libltv/gmp_ring_arithmetic.h"
}

TEST(GMPRingArithmeticTest, ModInv)
{
    Parameters* p = generateParameters(1024, 2, 1);
    mpz_t a;
    mpz_t a_inv;
    mpz_init(a);
    mpz_init(a_inv);

    mpz_urandomm(a, p->state, p->q);

    modInv(a_inv, a, p->q);

    mpz_mul(a, a, a_inv);
    mpz_mod(a, a, p->q);

    ASSERT_EQ(mpz_cmp_ui(a, 1), 0);

    mpz_clear(a);
    mpz_clear(a_inv);
}

TEST(GMPRingArithmeticTest, PolyAdd)
{
    Parameters* p_a = generateParameters(1024, 2, 1);
    Parameters* p_b = generateParameters(500, 2, 1);
    Polynomial output = { NULL, p_a->n };
    Polynomial a = { NULL, p_a->n };
    Polynomial b = { NULL, p_b->n };

    initPoly(&output);
    initPoly(&a);
    initPoly(&b);

    for(uint64_t i = 0; i < p_a->n; i++)
    {
        mpz_urandomm(a.co[i], p_a->state, p_a->q);
    }
    for(uint64_t i = 0; i < p_b->n; i++)
    {
        mpz_urandomm(b.co[i], p_b->state, p_b->q);
    }

    polyAdd(output, a, b);

    mpz_t check;
    mpz_init(check);
    for(uint64_t i = 0; i < p_a->n || i < p_b->n; i++)
    {
        mpz_set_ui(check, 0);
        if(i < p_a->n) mpz_add(check, check, a.co[i]);
        if(i < p_b->n) mpz_add(check, check, b.co[i]);
        ASSERT_EQ(mpz_cmp(check, output.co[i]), 0);
    }
    mpz_clear(check);

    freePoly(&output);
    freePoly(&a);
    freePoly(&b);
    free((Parameters*) p_a);
    free((Parameters*) p_b);
}

TEST(GMPRingArithmeticTest, PolySub)
{
    Parameters* p_a = generateParameters(1024, 2, 1);
    Parameters* p_b = generateParameters(500, 2, 1);
    Polynomial output = { NULL, p_a->n };
    Polynomial a = { NULL, p_a->n };
    Polynomial b = { NULL, p_b->n };

    initPoly(&output);
    initPoly(&a);
    initPoly(&b);


    for(uint64_t i = 0; i < p_a->n; i++)
    {
        mpz_urandomm(a.co[i], p_a->state, p_a->q);
    }
    for(uint64_t i = 0; i < p_b->n; i++)
    {
        mpz_urandomm(b.co[i], p_b->state, p_b->q);
    }

    polySub(output, a, b);

    mpz_t check;
    mpz_init(check);
    for(uint64_t i = 0; i < p_a->n || i < p_b->n; i++)
    {
        mpz_set_ui(check, 0);
        if(i < p_a->n) mpz_add(check, check, a.co[i]);
        if(i < p_b->n) mpz_sub(check, check, b.co[i]);
        ASSERT_EQ(mpz_cmp(check, output.co[i]), 0);
    }
    mpz_clear(check);

    freePoly(&output);
    freePoly(&a);
    freePoly(&b);
    free((Parameters*) p_a);
    free((Parameters*) p_b);
}

TEST(GMPRingArithmeticTest, RingReduce)
{
    Parameters* p = generateParameters(1024, 2, 1);
    Polynomial input = { NULL, 2042 };
    Polynomial output = { NULL, p->n };
    mpz_t input_bound;

    initPoly(&input);
    initPoly(&output);
    mpz_init(input_bound);

    mpz_mul_ui(input_bound, p->q, 2);

    for(uint64_t i = 0; i < 2042; i++)
    {
        mpz_urandomm(input.co[i], p->state, input_bound);
    }
    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(output.co[i], p->state, p->q);
    }

    ringReduce(output, input, p);

    ASSERT_TRUE(sizeof(output.co) <= sizeof(output.co[0]) * p->n);

    for(uint64_t i = 0; i < p->n; i++)
    {
        ASSERT_LE(mpz_cmp(output.co[i], p->q), 0);
    }

    freePoly(&input);
    freePoly(&output);
    mpz_clear(input_bound);
    free((Parameters*) p);
}

TEST(GMPRingArithmeticTest, RingAdd)
{
    Parameters* p = generateParameters(1024, 2, 1);
    Polynomial output = { NULL, p->n };
    Polynomial output_check = { NULL, p->n };
    Polynomial a = { NULL, p->n };
    Polynomial b = { NULL, p->n };

    initPoly(&output);
    initPoly(&output_check);
    initPoly(&a);
    initPoly(&b);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a.co[i], p->state, p->q);
        mpz_urandomm(b.co[i], p->state, p->q);
    }

    ringAdd(output, a, b, p);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_add(output_check.co[i], a.co[i], b.co[i]);
        mpz_mod(output_check.co[i], output.co[i], p->q);
        ASSERT_EQ(mpz_cmp(output.co[i], output_check.co[i]), 0);
    }

    freePoly(&output);
    freePoly(&output_check);
    freePoly(&a);
    freePoly(&b);
    free((Parameters*) p);
}

TEST(GMPRingArithmeticTest, RingSub)
{
    Parameters* p = generateParameters(1024, 2, 1);
    Polynomial output = { NULL, p->n };
    Polynomial output_check = { NULL, p->n };
    Polynomial a = { NULL, p->n };
    Polynomial b = { NULL, p->n };

    initPoly(&output);
    initPoly(&output_check);
    initPoly(&a);
    initPoly(&b);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a.co[i], p->state, p->q);
        mpz_urandomm(b.co[i], p->state, p->q);
    }

    ringSub(output, a, b, p);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_sub(output_check.co[i], a.co[i], b.co[i]);
        mpz_mod(output_check.co[i], output.co[i], p->q);
        ASSERT_EQ(mpz_cmp(output.co[i], output_check.co[i]), 0);
    }

    freePoly(&output);
    freePoly(&output_check);
    freePoly(&a);
    freePoly(&b);
    free((Parameters*) p);
}

TEST(GMPRingArithmeticTest, RingMul)
{
    Parameters* p = generateParameters(1024, 2, 1);
    Polynomial output = { NULL, p->n };
    Polynomial output_check = { NULL, p->n * 2 };
    Polynomial a = { NULL, p->n };
    Polynomial b = { NULL, p->n };

    initPoly(&output);
    initPoly(&output_check);
    initPoly(&a);
    initPoly(&b);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_urandomm(a.co[i], p->state, p->q);
        mpz_urandomm(b.co[i], p->state, p->q);
    }

    ringMul(output, a, b, p);

    for(uint64_t i = 0; i < p->n * 2; i++)
    {
        mpz_set_ui(output_check.co[i], 0);
    }

    mpz_t scratch;
    mpz_init(scratch);
    for(uint64_t i = 0; i < p->n; i++)
    {
        for(uint64_t j = 0; j < p->n; j++)
        {
            mpz_mul(scratch, a.co[i], b.co[j]);
            mpz_add(output_check.co[i+j], output_check.co[i+j], scratch);
            mpz_mod(output_check.co[i+j], output_check.co[i+j], p->q);
        }
    }

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_sub(output_check.co[i], output_check.co[i], output_check.co[i + p->n]);
        mpz_mod(output_check.co[i], output_check.co[i], p->q);
        ASSERT_EQ(mpz_cmp(output.co[i], output_check.co[i]), 0);
    }

    freePoly(&output);
    freePoly(&output_check);
    freePoly(&a);
    freePoly(&b);
    free((Parameters*) p);
}

TEST(GMPRingArithmeticTest, PolyDiv)
{
    Parameters* p = generateParameters(1024, 2, 1);
    Polynomial check = { NULL, 2 * p->n };
    Polynomial quotient = { NULL, p->n };
    Polynomial remainder = { NULL, p->n };
    Polynomial a = { NULL, p->n };
    Polynomial b = { NULL, p->n };

    initPoly(&check);
    initPoly(&quotient);
    initPoly(&remainder);
    initPoly(&a);
    initPoly(&b);

    int div = 0;

    while(div == 0)
    {
        for(uint64_t i = 0; i < p->n; i++)
        {
            mpz_urandomm(a.co[i], p->state, p->q);
            mpz_urandomm(b.co[i], p->state, p->q);
        }
        mpz_set_ui(b.co[p->n - 1], 0);
        mpz_set_ui(b.co[p->n - 2], 0);

        div = polyDiv(quotient, remainder, a, b, p->q);
    }

    polyMul(check, b, quotient);
    polyAdd(check, check, remainder);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_mod(check.co[i], check.co[i], p->q);
        ASSERT_EQ(mpz_cmp(a.co[i], check.co[i]), 0);
    }

    setPoly(a, 0);
    mpz_set_ui(a.co[0], 2);
    copyPoly(b, a);

    setPoly(check, 0);
    mpz_set_ui(check.co[0], 1);

    polyDiv(quotient, remainder, a, b, p->q);

    for(uint64_t i = 0; i < p->n; i++)
    {
        mpz_mod(quotient.co[i], quotient.co[i], p->q);
        ASSERT_EQ(mpz_cmp(quotient.co[i], check.co[i]), 0);
    }

    ASSERT_TRUE(zero(remainder));

    freePoly(&quotient);
    freePoly(&remainder);
    freePoly(&a);
    freePoly(&b);
    freePoly(&check);
    free((Parameters*) p);
}

TEST(GMPRingArithmeticTest, ExtEuclidean)
{
    Parameters* p = generateParameters(200, 2, 1);
    Polynomial check_a = { NULL, 2 * p->n };
    Polynomial check_b = { NULL, 2 * p->n };
    Polynomial a = { NULL, p->n };
    Polynomial b = { NULL, p->n };
    Polynomial u = { NULL, p->n };
    Polynomial v = { NULL, p->n };
    Polynomial d = { NULL, p->n };

    initPoly(&check_a);
    initPoly(&check_b);
    initPoly(&a);
    initPoly(&b);
    initPoly(&u);
    initPoly(&v);
    initPoly(&d);

    int done = 0;

    while(!done)
    {
        for(uint64_t i = 0; i < p->n; i++)
        {
            mpz_urandomm(a.co[i], p->state, p->q);
            mpz_urandomm(b.co[i], p->state, p->q);
        }

        done = extEuclidean(u, v, d, a, b, p->q);
    }

    polyMul(check_a, a, u);
    polyMul(check_b, b, v);
    polyAdd(check_a, check_a, check_b);

    ASSERT_FALSE(zero(d));
    for(uint64_t i = 0; i < d.order; i++)
    {
        mpz_mod(check_a.co[i], check_a.co[i], p->q);
        ASSERT_EQ(mpz_cmp(check_a.co[i], d.co[i]), 0);
    }

    freePoly(&a);
    freePoly(&b);
    freePoly(&u);
    freePoly(&v);
    freePoly(&d);
    freePoly(&check_a);
    freePoly(&check_b);
    free((Parameters*) p);
}

TEST(GMPRingArithmeticTest, RingInv)
{
    Parameters* p = generateParameters(200, 2, 1);
    Polynomial a = { NULL, p->n };
    Polynomial a_inv = { NULL, p->n };
    initPoly(&a);
    initPoly(&a_inv);

    int inverted = 0;

    while(!inverted)
    {
        for(uint64_t i = 0; i < a.order; i++)
        {
            mpz_urandomm(a.co[i], p->state, p->q);
        }
        inverted = ringInv(a_inv, a, p);
    }

    ringMul(a, a, a_inv, p);

    for(uint64_t i = 1 ; i < a.order; i++)
    {
        ASSERT_EQ(mpz_cmp_ui(a.co[i], 0), 0);
    }
    ASSERT_EQ(mpz_cmp_ui(a.co[0], 1), 0);

    freePoly(&a);
    freePoly(&a_inv);
    free((Parameters*) p);
}
