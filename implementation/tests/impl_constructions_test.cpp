#include <gtest/gtest.h>
#include <gmp.h>
#include <limits.h>

extern "C" {
#include "../libltv/impl_constructions.h"
}

TEST(ImplConstructionsTest, StreamAdder)
{
    Parameters* p = generateParameters(8, 2, 3 * 64);
    KeyPair* keys = KeyGen(p);
    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);
    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    uint64_t a = gmp_urandomm_ui(p->state, ULONG_MAX / 2);
    uint64_t b = gmp_urandomm_ui(p->state, ULONG_MAX / 2);

    CipherTextSet* e_a = decomposeEnc(a, keys->pk, p, 64);
    CipherTextSet* e_b = decomposeEnc(b, keys->pk, p, 64);
    CipherTextSet* e_out = decomposeEnc(0, keys->pk, p, 64);

    streamAdder(e_out, *e_a, *e_b, hint, p);

    uint64_t out = composeDec(e_out, keys->sk, p);

    ASSERT_EQ(out, a + b);

    freeCipherTextSet(e_a);
    freeCipherTextSet(e_b);
}

TEST(ImplConstructionsTest, FullAdder)
{
    Parameters* p = generateParameters(256, 2, 3);
    KeyPair* keys = KeyGen(p);

    char char_a = gmp_urandomm_ui(p->state, 2);
    char char_b = gmp_urandomm_ui(p->state, 2);
    char char_c_in = gmp_urandomm_ui(p->state, 2);
    char char_s, char_c_out;
    PlainText* a = newPlainText(p->n);
    PlainText* b = newPlainText(p->n);
    PlainText* s = newPlainText(p->n);
    PlainText* c_in = newPlainText(p->n);
    PlainText* c_out = newPlainText(p->n);
    DblCRTCipherText e_s = newDblCRTCipherText(p);
    DblCRTCipherText e_c_out = newDblCRTCipherText(p);

    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);

    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    setPoly(*a, 0);
    setPoly(*b, 0);
    setPoly(*c_in, 0);
    mpz_set_ui(a->co[0], char_a);
    mpz_set_ui(b->co[0], char_b);
    mpz_set_ui(c_in->co[0], char_c_in);

    DblCRTCipherText e_a = CRTEnc(a, keys->pk, p);
    DblCRTCipherText e_b = CRTEnc(b, keys->pk, p);
    DblCRTCipherText e_c_in = CRTEnc(c_in, keys->pk, p);

    fullAdder(&e_s, &e_c_out, e_a, e_b, e_c_in, hint, p);

    CRTDec(s, e_s, keys->sk, p);
    CRTDec(c_out, e_c_out, keys->sk, p);

    char_s = mpz_get_ui(s->co[0]);
    char_c_out = mpz_get_ui(c_out->co[0]);

    ASSERT_EQ(char_s, char_a ^ char_b ^ char_c_in);
    ASSERT_EQ(char_c_out, (char_a & char_b) | ((char_a ^ char_b) & char_c_in));

    freePlainText(a);
    freePlainText(b);
    freePlainText(s);
    freePlainText(c_in);
    freePlainText(c_out);
    freeDblCRTCipherText(e_a);
    freeDblCRTCipherText(e_b);
    freeDblCRTCipherText(e_c_in);
    freeParameters(p);
}

TEST(ImplConstructionsTest, ComparisonInt)
{
    // very low ring dimension for a fast running test
    Parameters* p = generateParameters(8, 2, 100);
    KeyPair* keys = KeyGen(p);
    PlainText* m = newPlainText(p->n);
    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);

    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    for(int i = 0; i < 10; i++)
    {
        uint64_t a = gmp_urandomm_ui(p->state, ULONG_MAX);
        uint64_t b = gmp_urandomm_ui(p->state, ULONG_MAX);

        CipherTextSet* c = decomposeEnc(a, keys->pk, p, 64);

        DblCRTCipherText enc_result = compareGreaterThanInt(c, b, keys->pk, hint, p);

        CRTDec(m, enc_result, keys->sk, p);

        for(uint64_t j = 1; j < p->n; j++)
        {
            ASSERT_EQ(mpz_cmp_ui(m->co[j], 0), 0);
        }

        int result = mpz_get_ui(m->co[0]);

        ASSERT_TRUE(result == 0 || result == 1);

        if(result == 1)
        {
            printf("[%d] : %lu > %lu : %d\n", i, a, b, result);
            ASSERT_TRUE(a > b);
        }

        if(result == 0)
        {
            printf("[%d] : %lu <= %lu : %d\n", i, a, b, result);
            ASSERT_TRUE(a <= b);
        }
    }


    freePoly(&joint_sk);
    freeDblCRT(hint);
    freePlainText(m);
    freeParameters(p);
}

TEST(ImplConstructionsTest, StreamAdderBit)
{
    Parameters* p = generateParameters(8, 2, 3 * 64);
    KeyPair* keys = KeyGen(p);
    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);
    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    uint64_t a = gmp_urandomm_ui(p->state, ULONG_MAX);
    uint64_t b = gmp_urandomm_ui(p->state, 2);

    CipherTextSet* e_a = decomposeEnc(a, keys->pk, p, 64);
    CipherTextSet* e_b = decomposeEnc(b, keys->pk, p, 64);
    CipherTextSet* e_out = decomposeEnc(0, keys->pk, p, 64);

    streamAdderBit(e_out, *e_a, e_b->values[0], hint, keys->pk, p);

    uint64_t out = composeDec(e_out, keys->sk, p);

    ASSERT_EQ(out, a + b);

    composeDec(e_a, keys->sk, p);
    composeDec(e_b, keys->sk, p);

    freePoly(&joint_sk);
    freeParameters(p);
}

TEST(ImplConstructionsTest, StreamAdderRepeated)
{
    Parameters* p = generateParameters(8, 2, 3 * 10 * 8);
    KeyPair* keys = KeyGen(p);
    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);
    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    for(int j = 0; j < 3; j++)
    {
        uint64_t a = 0;
        uint64_t b = 1;
        uint64_t total = 0;

        CipherTextSet* e_a = decomposeEnc(a, keys->pk, p, 8);
        CipherTextSet* e_b = decomposeEnc(b, keys->pk, p, 8);
        for(int i = 0; i < j; i++)
        {
            streamAdder(e_a, *e_a, *e_b, hint, p);
            total += b;
        }
        uint64_t out = composeDec(e_a, keys->sk, p);
        ASSERT_EQ(out, total);
        composeDec(e_b, keys->sk, p);
    }

    freePoly(&joint_sk);
    freeParameters(p);
}

TEST(ImplConstructionsTest, StreamAdderBitRepeated)
{
    Parameters* p = generateParameters(8, 2, 3 * 10 * 8);
    KeyPair* keys = KeyGen(p);
    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);
    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    for(int j = 0; j < 3; j++)
    {
        uint64_t a = 0;
        uint64_t b = 1;
        uint64_t total = 0;

        CipherTextSet* e_a = decomposeEnc(a, keys->pk, p, 8);
        CipherTextSet* e_b = decomposeEnc(b, keys->pk, p, 8);
        for(int i = 0; i < j; i++)
        {
            streamAdderBit(e_a, *e_a, e_b->values[0], hint, keys->pk, p);
            total += b;
        }
    	uint64_t out = composeDec(e_a, keys->sk, p);
    	ASSERT_EQ(out, total);
        composeDec(e_b, keys->sk, p);
    }
    freePoly(&joint_sk);
    freeParameters(p);
}


TEST(ImplConstructionsTest, InitCipherTextSet)
{
    Parameters* p = generateParameters(128, 2, 1);
    CipherTextSet* c;
    initCipherTextSet(&c, 64, p);
    ASSERT_EQ(c->max_index, 64);
    freeCipherTextSet(c);
    freeParameters(p);
}

TEST(ImplConstructionsTest, Correctness)
{
    Parameters* p = generateParameters(128, 2, 1);
    KeyPair* keys = KeyGen(p);

    uint64_t m = gmp_urandomm_ui(p->state, ULONG_MAX);

    CipherTextSet* c = decomposeEnc(m, keys->pk, p, 64);
    uint64_t m_prime = composeDec(c, keys->sk, p);

    ASSERT_EQ(m, m_prime);

    freeParameters(p);
}

TEST(ImplConstructionsTest, HalfAdder)
{
    Parameters* p = generateParameters(256, 2, 2);
    KeyPair* keys = KeyGen(p);

    char char_a = gmp_urandomm_ui(p->state, 2);
    char char_b = gmp_urandomm_ui(p->state, 2);
    char char_s, char_c;
    PlainText* a = newPlainText(p->n);
    PlainText* b = newPlainText(p->n);
    PlainText* s = newPlainText(p->n);
    PlainText* c = newPlainText(p->n);
    DblCRTCipherText e_s = newDblCRTCipherText(p);
    DblCRTCipherText e_c = newDblCRTCipherText(p);

    Polynomial joint_sk = { NULL, p->n };
    initPoly(&joint_sk);

    ringMul(joint_sk, keys->sk, keys->sk, p);
    DblCRT hint = CRTKeySwitchHint(2, joint_sk, keys->sk, p);

    setPoly(*a, 0);
    setPoly(*b, 0);
    mpz_set_ui(a->co[0], char_a);
    mpz_set_ui(b->co[0], char_b);

    DblCRTCipherText e_a = CRTEnc(a, keys->pk, p);
    DblCRTCipherText e_b = CRTEnc(b, keys->pk, p);

    halfAdder(&e_s, &e_c, e_a, e_b, hint, p);

    CRTDec(s, e_s, keys->sk, p);
    CRTDec(c, e_c, keys->sk, p);

    char_s = mpz_get_ui(s->co[0]);
    char_c = mpz_get_ui(c->co[0]);

    ASSERT_EQ(char_s, char_a ^ char_b);
    ASSERT_EQ(char_c, char_a & char_b);

    freePlainText(a);
    freePlainText(b);
    freePlainText(s);
    freePlainText(c);
    freeDblCRTCipherText(e_a);
    freeDblCRTCipherText(e_b);
    freeParameters(p);
}
